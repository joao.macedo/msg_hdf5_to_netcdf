## msg2nc.py
The msg2nc python module is a format converter.
It converts MeteoSAT-MSG products from native HDF5 file format into netCDF4 CF-compliance.
The Geostationary projection Metadata was defined as described in the NetCDF Climate and
Forecast (CF) Metadata Conventions Version 1.6 (Appendix F: Grid Mappings). The projection
parameters computation was based in the LRIT/HRIT Global Specification (_Written by EUMETSAT on behalf of CGMS_).
In addition the MISSING_VALUE, OFFSET, SCALING_FACTOR and UNITS attributes were replaced by the
Cf-conventions respective attributes (_FillValue, offset, scale_factor, units). Remainding that
the scale_factor is the inverse value of the original SCALING_FACTOR.
All the remaining attributes are being removed.

DOWNLOAD and INSTALL:
```
git clone https://gitlab.com/joao.macedo/msg_hdf5_to_netcd.git
cd msg_hdf5_to_netcdf
pip install .
```
Alternatively to pip (python installer) you can run ```python setup.py```. 

Use <code>msg2nc --help</code> to see the following documentation:
```
usage: msg2nc [-h] [-v [VARIABLES [VARIABLES ...]]]
              [-x [EXCEPT_VARIABLES [EXCEPT_VARIABLES ...]]]
              [-c COMPRESSION_LEVEL] [-t] [-d]
              input_path output_path

examples:
---------

Convert the MSG (HDF5) Land Surface Temperature (LST) product into NetCDF4.
Extract only the "LST" and "Q_FLAGS" variables, with internal compression
level = 6 (GZIP) and create the additional time dimension:

~$msg2nc data/HDF5_LSASAF_MSG_LST_MSG-Disk_201605200345.h5 data/NC_LSASAF_MSG_LST-QFLAGS_MSG-Disk_201605200345.nc -v LST Q_FLAGS -c 6 -t

Extract all Datasets from the Cloud Mask (CMA) product, which already is a
netCDF4, except the "nx", "ny", "lat", "lon" and the ones how match the
"^pal.*" and ".*pal$" regular expressions, without internal compression:

~$msg2nc data/NetCDF_NWC_CMA_MSG4_MSG-Disk_201811141200 data/NetCDF_NWC_CMa_MSG4_MSG-Disk_201811141200.nc -x nx ny lat lon ^pal.* .*pal$ -c 0




positional arguments:
  input_path            Path to MSG (HDF5) file.
  output_path           Path to MSG (NetCDF4) file.

optional arguments:
  -h, --help            show this help message and exit
  -v [VARIABLES [VARIABLES ...]], --variables [VARIABLES [VARIABLES ...]]
                        Name of the Dataset(s) to be extracted from the (HDF5)
                        input file. Also regex (regular expressions) can be
                        given. Use flag -d to disable regex interpretation if
                        needed.
  -x [EXCEPT_VARIABLES [EXCEPT_VARIABLES ...]], --except-variables [EXCEPT_VARIABLES [EXCEPT_VARIABLES ...]]
                        Name of the Dataset(s) to be excluded from the input
                        file. Also regex (regular expressions) can be given.
                        Use flag -d to disable regex interpretation if needed.
  -c COMPRESSION_LEVEL, --compression-level COMPRESSION_LEVEL
                        Output internal compression level (GZIP) [0-9].
  -t, --with-time-dim   Create additional time dimension. The created time
                        dimension is Unlimited, allowing the user to extend it
                        if necessary.
  -d, --disable-regex   Disables regex (regular expressions) interpretation of
                        the variable/dataset names, if the -v and/or -x flags
                        are being used.
```
