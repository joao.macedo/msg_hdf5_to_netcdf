#!/usr/bin/env python3
#-*- coding:utf-8 -*-

# MIT License
# 
# Copyright (c) 2018 likeno
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""
    The msg2nc python module is a format converter.
It converts MeteoSAT-MSG products from native HDF5 file format into
netCDF4 CF-compliance.
    The Geostationary projection Metadata was defined as described in the
NetCDF Climate and Forecast (CF) Metadata Conventions Version 1.6
(Appendix F: Grid Mappings).
    The projection parameters computation was based in the LRIT/HRIT Global
Specification (Written by EUMETSAT on behalf of CGMS).
    In addition the MISSING_VALUE, OFFSET, SCALING_FACTOR and UNITS attributes
were replaced by the Cf-conventions respective attributes (_FillValue,
add_offset, scale_factor, units).
    Remainding that the scale_factor is the inverse value of the original
SCALING_FACTOR. All the remaining attributes are being removed.

examples:
---------

Convert the MSG (HDF5) Land Surface Temperature (LST) product into NetCDF4.
Extract only the "LST" and "Q_FLAGS" variables, with internal compression
level = 6 (GZIP) and create the additional time dimension:

~$msg2nc data/HDF5_LSASAF_MSG_LST_MSG-Disk_201605200345.h5 data/NC_LSASAF_MSG_LST-QFLAGS_MSG-Disk_201605200345.nc -v LST Q_FLAGS -c 6 -t


Extract all Datasets from the Cloud Mask (CMA) product, which already is a
netCDF4, except the "nx", "ny", "lat", "lon" and the ones how match the
"^pal.*" and ".*pal$" regular expressions, without internal compression:

~$msg2nc data/NetCDF_NWC_CMA_MSG4_MSG-Disk_201811141200 data/NetCDF_NWC_CMa_MSG4_MSG-Disk_201811141200.nc -x nx ny lat lon ^pal.* .*pal$ -c 0

"""
import argparse
from collections import OrderedDict
import datetime as dt
import h5py
import logging
import numpy as np
import os
import re

logger = logging.getLogger(__name__)

# geostationary projection parameters for MSG Disk:
CENTER_OF_EARTH_DISTANCE = 42164     # km
EQUATORIAL_RADIUS        = 6378137   # m
POLAR_RADIUS             = 6356752.3 # m
SATELLITE_HEIGHT = CENTER_OF_EARTH_DISTANCE * 1E3 - EQUATORIAL_RADIUS
INVERSE_FLATTENING = EQUATORIAL_RADIUS / (EQUATORIAL_RADIUS - POLAR_RADIUS)

# Default parameters for MSG Disk:
COFF = 1857
LOFF = 1857
CFAC = 13642337
LFAC = 13642337
NC = 3712
NL = 3712

CF_ATTRIBUTES = [
    'valid_range', 'add_offset', '_FillValue', 'scale_factor',
    'flag_meanings', 'flag_masks', 'flag_values', 'units',
    'CellMethods', 'Conventions', 'title', 'standard_name',
    'long_name', 'comment', 'source', 'references', 'history',
    'institution'
]


def convert_h5_to_nc(input_path, output_path, crop_bounds=[None, None, None, None],
                     with_time=None, compression_level=9,
                     msg_full_disk=False, variables=[],
                     except_variables=[], disable_regex=False,
                     coords_in_meters=False):

    logger.info("Convert MSG HDF5 input file: {} "
                 "into a georeferenced NetCDF file: {}"
                 "".format(input_path, output_path))
    landsaf_hdf5 = h5py.File(input_path, 'r')
    landsaf_nc = h5py.File(output_path,'w')

    # Set crop image parameters:
    if all(crop_bounds):
        # Convert the default fotran array col/row index (start=1)
        # to Python/C index (start=0)
        crop_bounds = [crop_bounds[0]-1, crop_bounds[1],
                       crop_bounds[2]-1, crop_bounds[3]]
        row_num = crop_bounds[1] - crop_bounds[0]
        col_num = crop_bounds[3] - crop_bounds[2]
    else:
        row_num = (NL if msg_full_disk
                   else  landsaf_hdf5.attrs.get('NL', NL))
        col_num = (NC if msg_full_disk
                   else  landsaf_hdf5.attrs.get('NC', NC))
        crop_bounds = [0, row_num, 0, col_num]

    # Temporary condition for raster layers:
    is_raster = True

    create_dimensions(landsaf_hdf5, landsaf_nc, crop_bounds,
                      with_time, msg_full_disk, coords_in_meters)
    try:
        re_pattern = re.compile(b'.*(\(|<)(.*)(\)|>)')
        subsat_lon = float(
            re_pattern.sub('\g<2>', landsaf_hdf5.attrs['PROJECTION_NAME'])
        )
    except (ValueError, TypeError):
        subsat_lon = 0.0

    create_grid_mapping(landsaf_nc, subsat_lon)

    landsaf_nc.attrs.create('Conventions', _to_np_dtype("CF-1.6"))

    for attr_name in landsaf_hdf5.attrs.keys():
        if attr_name in CF_ATTRIBUTES:
            landsaf_nc.attrs.create(
                attr_name, _to_np_dtype(landsaf_hdf5.attrs[attr_name])
            )

    vars_re_list = [re.compile(pattern) for pattern in variables]
    xvars_re_list = [re.compile(pattern) for pattern in except_variables]

    for ds_name, h5dset in landsaf_hdf5.items():
        if disable_regex:
            process_dataset = (
                (not any(variables) or ds_name in variables) and
                ds_name not in except_variables)
        else:
            process_dataset = (
                (not any(variables) or
                 any([re_patt.match(ds_name).group() == ds_name
                      for re_patt in vars_re_list
                      if re_patt.match(ds_name)])) and
                not any([re_patt.match(ds_name).group() == ds_name
                         for re_patt in xvars_re_list
                         if re_patt.match(ds_name)]))

        if process_dataset:

            logger.info("processing variable: {}".format(ds_name))
            creation_props = {}
            if compression_level != 0:
                creation_props.update({"compression": "gzip",
                                       "compression_opts": compression_level})

            ds_data = h5dset[crop_bounds[0]: crop_bounds[1],
                             crop_bounds[2]: crop_bounds[3]]
            if with_time is not None:
                ds_data = ds_data.reshape([1, row_num, col_num])
                creation_props.update({'maxshape': (None, row_num, col_num)})

            mapped_attrs = _remap_var_attrs(h5dset)

            fill_value = mapped_attrs.get('_FillValue')
            if fill_value is not None:
                creation_props["fillvalue"] = fill_value

            ncvar = landsaf_nc.create_dataset(
                ds_name, ds_data.shape, ds_data.dtype, ds_data,
                **creation_props
            )

            if is_raster:
                ncvar.attrs.create('grid_mapping',_to_np_dtype('geostationary'))
                if with_time is not None:
                    ncvar.attrs.create('coordinates', _to_np_dtype("time y x"))
                else:
                    ncvar.attrs.create('coordinates', _to_np_dtype("y x"))

            for attr_name, attr_value in mapped_attrs.items():
                ncvar.attrs.create(attr_name, attr_value)

            if is_raster:
                if with_time is not None:
                    ncvar.dims[0].attach_scale(landsaf_nc['time'])
                    ncvar.dims[1].attach_scale(landsaf_nc['y'])
                    ncvar.dims[2].attach_scale(landsaf_nc['x'])
                else:
                    ncvar.dims[0].attach_scale(landsaf_nc['y'])
                    ncvar.dims[1].attach_scale(landsaf_nc['x'])

    landsaf_nc.close()
    landsaf_hdf5.close()


def _remap_var_attrs(h5dset):
    remap_units = {
        'Degrees Celsium': 'degree_Celsius',
        'Deg.': 'degree'
    }

    if h5dset.name == '/LAT':
        remap_units['Deg.'] = "degree_north"
    elif h5dset.name == '/LON':
        remap_units['Deg.'] = "degree_east"

    remap_attrs = OrderedDict([
        ('SCALING_FACTOR', {'name':'scale_factor',
                            'value': lambda x: 1./x if x != 1 else None}),
        ('OFFSET',         {'name': 'add_offset',
                            'value': lambda x: (
            x if x != 0 else None
        )}),
        ('MISSING_VALUE',  {'name': '_FillValue'}),
        ('UNITS',          {'name': 'units', 'value': lambda a: (
            remap_units.get(a.decode() if hasattr(a, 'decode') else a, a)
        )}),
    ])

    result = {}
           
    for raw_name, raw_value in h5dset.attrs.items():
        attr_value = None
        if raw_name in CF_ATTRIBUTES:
            attr_name = raw_name
            attr_value = raw_value
        if attr_value is not None:
            var_dtype = h5dset.dtype if attr_name in (
                '_FillValue', 'valid_range', 'flag_values'
            ) else None
            result.update({attr_name: _to_np_dtype(attr_value, var_dtype)})

    for raw_name, remap_dict in remap_attrs.items():
        attr_value = None
        raw_value = h5dset.attrs.get(raw_name)
        if raw_value is not None:
            attr_name = remap_dict.get('name', raw_name)
            attr_value = remap_dict.get('value', lambda x: x)(raw_value)
        if attr_value is not None:
            var_dtype = h5dset.dtype if attr_name in (
                '_FillValue', 'valid_range', 'flag_values'
            ) else None
            result.update({attr_name: _to_np_dtype(attr_value, var_dtype)})
 
    return result


def get_xy_scale_offset(cfac, coff, lfac, loff,
                        coords_in_meters=False):

    aux16 = 2**(-16)
    x_scale = np.deg2rad(1.0 / (aux16 * cfac))
    y_scale = np.deg2rad(1.0 / (aux16 * lfac)) * -1.0
    if coords_in_meters:
        x_scale *= SATELLITE_HEIGHT
        y_scale *= SATELLITE_HEIGHT
    x_offset = (1 - coff) * x_scale
    y_offset = (1 - loff) * y_scale

    return (x_scale, x_offset, y_scale, y_offset)


def create_dimensions(landsaf_hdf5, landsaf_nc, crop_bounds,
                      with_time=None, msg_full_disk=False,
                      coords_in_meters=False, encode_coords=False):

    if with_time is not None:
        if any(with_time):
            image_reference_time = dt.datetime.strptime(
                with_time, "%Y-%m-%dT%H:%M:%S"
            )
        else:
            try:
                time_str = landsaf_hdf5.attrs['IMAGE_ACQUISITION_TIME'].decode()
            except AttributeError:
                time_str = landsaf_hdf5.attrs['IMAGE_ACQUISITION_TIME']
            except KeyError:
                time_str = os.path.basename(landsaf_hdf5.filename).split('_')[5]+"00"
            image_reference_time = dt.datetime.strptime(
                time_str, "%Y%m%d%H%M%S"
            )

        time_attrs = {
            'long_name': "time",
            'axis': "T",
            'calendar': "gregorian",
            'NAME': "Zaxis",
            'units': image_reference_time.strftime(
                "hours since %Y-%m-%d %H:%M:00"
            ),
        }

        time = landsaf_nc.create_dataset('time', (1,), np.int32, (0,))
        for attr_name, attr_value in time_attrs.items():
            time.attrs.create(attr_name, _to_np_dtype(attr_value))

    coff = (COFF if msg_full_disk
            else landsaf_hdf5.attrs.get('COFF', COFF))
    cfac = (CFAC if msg_full_disk
            else  landsaf_hdf5.attrs.get('CFAC', CFAC))
    loff = (LOFF if msg_full_disk
            else  landsaf_hdf5.attrs.get('LOFF', LOFF))
    lfac = (LFAC if msg_full_disk
            else  landsaf_hdf5.attrs.get('LFAC', LFAC))

    (x_scale, x_offset, y_scale, y_offset) = get_xy_scale_offset(
        cfac, coff, lfac, loff, coords_in_meters
    )

    y_attrs = {
        'standard_name': "projection_y_coordinate",
        'long_name': "y coordinate of projection",
        'axis': "Y",
        'NAME': "Yaxis",
        'units': "m" if coords_in_meters else "radian",
    }
    x_attrs = {
        'standard_name': "projection_x_coordinate",
        'long_name': "x coordinate of projection",
        'axis': "X",
        'NAME': "Xaxis",
        'units': "m" if coords_in_meters else "radian",
    }

    yc = np.arange(*crop_bounds[:2], dtype='i2')
    xc = np.arange(*crop_bounds[2:], dtype='i2')

    if encode_coords:
        y_attrs.update({
            'scale_factor': y_scale,
            'add_offset': y_offset,
        })
        x_attrs.update({
            'scale_factor': x_scale,
            'add_offset': x_offset,
        })
    else:
        yc = np.array((yc * y_scale) + y_offset, 'f8')
        xc = np.array((xc * x_scale) + x_offset, 'f8')

    y = landsaf_nc.create_dataset('y', (yc.size,), yc.dtype, yc)
    for attr_name, attr_value in y_attrs.items():
        y.attrs.create(attr_name, _to_np_dtype(attr_value))

    x = landsaf_nc.create_dataset('x', (xc.size,), xc.dtype, xc)
    for attr_name, attr_value in x_attrs.items():
        x.attrs.create(attr_name, _to_np_dtype(attr_value))


def create_grid_mapping(landsaf_nc, subsat_lon):

    grid_mapping_attrs = {
        'grid_mapping_name': 'geostationary',
        'longitude_of_prime_meridian': 0.0,
        'longitude_of_projection_origin': subsat_lon,
        'false_easting': 0.0,
        'false_northing': 0.0,
        'semi_major_axis': EQUATORIAL_RADIUS,
        'inverse_flattening': INVERSE_FLATTENING,
        'sweep_angle_axis': "y",
        'perspective_point_height': SATELLITE_HEIGHT,
    }

    grid_mapping = landsaf_nc.create_dataset('geostationary', (1,), np.dtype('S1'))
    for attr_name, attr_value in grid_mapping_attrs.items():
        grid_mapping.attrs.create(attr_name, _to_np_dtype(attr_value))


def _to_np_dtype(raw_value, dtype=None):
    if (isinstance(raw_value, float) or
        isinstance(raw_value, int)):
        value = np.array([raw_value])
    else:
        value = np.array(raw_value, dtype)
    if value.dtype.kind == 'U':
        value = np.array(value.item().encode('utf8'), '|S')
    return value

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__
    )
    parser.add_argument("input_path",
                        help="Path to MSG (HDF5) file.")
    parser.add_argument("output_path",
                         help="Path to MSG (NetCDF4) file.")
    parser.add_argument("-b", "--crop-bounds", type=int, nargs=4,
                        action='store', default=[None, None, None, None],
                        help="Crop MSG image according with the following "
                        "bounds [start_row, end_row, start_col, end_col]. "
                        "Default value corresponds to the original image area "
                        "(i.e. not croped).")
    parser.add_argument("-t", "--with-time", nargs='?', const='',
                         help="Create additional time dimension. "
                         "Use a datetime string (fmt: YYYY-mm-ddTHH:MM:SS) "
                         "to define the image reference time. "
                         "This flag can also be used without any value, "
                         "in that case the image reference time will be "
                         "retrieved from the IMAGE_ACQUISITION_TIME attribute "
                         "or from the input filename (in case it follows "
                         "the LSASAF standard file naming). "
                         "The created time dimension is Unlimited, "
                         "allowing the user to extend it if necessary.")
    parser.add_argument("-c", "--compression-level", type=int, default=9,
                         help="Output internal compression level "
                         "(GZIP) [0-9].")
    parser.add_argument("-m", "--coords-in-meters", action="store_true",
                         help="Generate geostationary coordinates in "
                              "meters. (Default unit is radian)")
    parser.add_argument("-fd", "--msg-full-disk", action="store_true",
                         help="Force MSG Full-Disk parameters as default. "
                         "This flag can be used to overwrite the input file's "
                         "metadata (LOFF, COFF, LFAC, CFAC, NC, NL) with the "
                         "default MSG Full-Disk parameters. ATENTION: It "
                         "should only be used if the input file's respective "
                         "parameters are wrong or missing and the file's "
                         "image corresponds to the MGG Full-Disk area.")
    parser.add_argument("-v", "--variables", nargs='*', action='store',
                        default=[], help="Name of the Dataset(s) to be "
                        "extracted from the (HDF5) input file. Also regex "
                        "(regular expressions) can be given. Use flag -d to "
                        "disable regex interpretation if needed.")
    parser.add_argument("-x", "--except-variables", nargs='*', action='store',
                        default=[], help="Name of the Dataset(s) to be "
                        "excluded from the input file. Also regex "
                        "(regular expressions) can be given. Use flag -d to "
                        "disable regex interpretation if needed.")
    parser.add_argument("-d", "--disable-regex", action="store_true",
                         help="Disables regex (regular expressions) "
                         "interpretation of the variable/dataset names, if "
                         "the -v and/or -x flags are being used.")

    args = parser.parse_args()
    convert_h5_to_nc(**vars(args))

    
if __name__ == '__main__':
   main() 
