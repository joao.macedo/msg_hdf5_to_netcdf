#!/usr/bin/env python
#-*- coding:utf-8 -*-

from osgeo import osr
import numpy as np


def get_msg_geos_coords():
    """Compute xy geostationary (MSG) coordinates in meters
       according with the LRIT/HRIT Global Specification document
    """
    # geostationary projection parameters for MSG Disk:

    # Old values from the CGMS HRIT/LRIT GLOBAL SPECIFICATION document:
    EQUATORIAL_RADIUS        = 6378169.0 # m
    POLAR_RADIUS             = 6356583.8 # m

    # The values above should be replaced by the following (WGS84 datum):
    #EQUATORIAL_RADIUS        = 6378137   # m
    #POLAR_RADIUS             = 6356752.3 # m

    # Details can be found in the AMENDMENT TO THE CGMS LRIT/HRIT GLOBAL
    # SPECIFICATION:
    # https://www.cgms-info.org/documents/documents-cgms/cwpt_706~2.pdf

    CENTER_OF_EARTH_DISTANCE = 42164 # km

    SATELLITE_HEIGHT = CENTER_OF_EARTH_DISTANCE * 1E3 - EQUATORIAL_RADIUS
    
    # Default parameters for MSG Disk:
    COFF = 1857
    LOFF = 1857
    CFAC = 13642337
    LFAC = 13642337
    NC = 3712
    NL = 3712

    aux16 = 2**(-16)

    x_scale = np.deg2rad(1.0 / (aux16 * CFAC)) * SATELLITE_HEIGHT
    x_offset = (1 - COFF) * x_scale
    xc = np.arange(NC) * x_scale + x_offset 

    y_scale = np.deg2rad(1.0 / (aux16 * LFAC)) * SATELLITE_HEIGHT * -1.0
    y_offset = (1 - LOFF) * y_scale
    yc = np.arange(NL) * y_scale + y_offset 

    return xc, yc


def get_msg_latlon():

    xc, yc = get_msg_geos_coords()

    # Spread x/y coordinates into a 2D array:
    xx = np.array(xc).repeat(yc.size,0).reshape((yc.size, xc.size)).T
    yy = np.array(yc).repeat(xc.size,0).reshape(xx.shape)

    geos_proj = osr.SpatialReference()
    geos_proj.ImportFromProj4('+proj=geos +lon_0=0 +h=35785831. '
                              '+x_0=0 +y_0=0 +a=6378169.0 +b=6356583.8 '
                              '+units=m +sweep=y +no_defs')

    latlon_proj = osr.SpatialReference()
    latlon_proj.ImportFromProj4("+proj=latlong +ellps=WGS84 +datum=WGS84")

    # Compute latlon with Proj library:
    coords_transf = osr.CoordinateTransformation(geos_proj, latlon_proj)

    geos_coords = np.array([xx.ravel(), yy.ravel(), np.zeros(xx.size)]).T
    latlon_coords = coords_transf.TransformPoints(geos_coords)
    lon, lat = np.array(latlon_coords).T[:2]

    return lat.reshape(xx.shape), lon.reshape(xx.shape)
